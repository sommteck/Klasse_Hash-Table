using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections;       // Zum Einbinden der Klasse für Hash-Tables

namespace Klasse_Hash_Table_GUI
{
    public partial class Klasse_Hash_Table : Form
    {
        Hashtable table = new Hashtable();

        public Klasse_Hash_Table()
        {
            InitializeComponent();

            table["Eins"] = 13;
            table["Zwei"] = "C# ist auch eher so meh!";
            table["Drei"] = 23.42;
            table["Vier"] = '?';
            table["Fuenf"] = true;

            /*
            table["1"] = 13;
            table["2"] = "C# ist auch eher so meh!";
            table["3"] = 23.42;
            table["4"] = '?';
            table["5"] = true;
            */
            
        }

        private void cmd_end_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void cmd_clear_Click(object sender, EventArgs e)
        {
            dataGridView1.Rows.Clear();
        }

        private void cmd_show_Click(object sender, EventArgs e)
        {
            dataGridView1.Rows.Clear();

            foreach (DictionaryEntry de in table)
                dataGridView1.Rows.Add(de.Key, de.Value);
        }

        private void cmd_one_show_Click(object sender, EventArgs e)
        {
            dataGridView1.Rows.Clear();

            dataGridView1.Rows.Add("Zwei", table["Zwei"]);

            /*
            dataGridView1.Rows.Add("2", table["2"]);
            */
        }

        private void cmd_add_element_Click(object sender, EventArgs e)
        {
            try
            {
                table.Add("Sechs", "eingefügtes Element");

                foreach (DictionaryEntry de in table)
                    dataGridView1.Rows.Add(de.Key, de.Value);
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void cmd_remove_element_Click(object sender, EventArgs e)
        {
            dataGridView1.Rows.Clear();


            table.Remove("Eins");

            /*
            table.Remove("1");
            */

            foreach (DictionaryEntry de in table)
                dataGridView1.Rows.Add(de.Key, de.Value);
        }
    }
}