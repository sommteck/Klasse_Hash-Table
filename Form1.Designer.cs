namespace Klasse_Hash_Table_GUI
{
    partial class Klasse_Hash_Table
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.cmd_end = new System.Windows.Forms.Button();
            this.cmd_clear = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.LN_Schluessel = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LN_Wert = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cmd_show = new System.Windows.Forms.Button();
            this.cmd_one_show = new System.Windows.Forms.Button();
            this.cmd_add_element = new System.Windows.Forms.Button();
            this.cmd_remove_element = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // cmd_end
            // 
            this.cmd_end.Location = new System.Drawing.Point(458, 359);
            this.cmd_end.Name = "cmd_end";
            this.cmd_end.Size = new System.Drawing.Size(75, 32);
            this.cmd_end.TabIndex = 0;
            this.cmd_end.Text = "&Ende";
            this.cmd_end.UseVisualStyleBackColor = true;
            this.cmd_end.Click += new System.EventHandler(this.cmd_end_Click);
            // 
            // cmd_clear
            // 
            this.cmd_clear.Location = new System.Drawing.Point(332, 359);
            this.cmd_clear.Name = "cmd_clear";
            this.cmd_clear.Size = new System.Drawing.Size(75, 32);
            this.cmd_clear.TabIndex = 1;
            this.cmd_clear.Text = "&Löschen";
            this.cmd_clear.UseVisualStyleBackColor = true;
            this.cmd_clear.Click += new System.EventHandler(this.cmd_clear_Click);
            // 
            // dataGridView1
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.LN_Schluessel,
            this.LN_Wert});
            this.dataGridView1.Location = new System.Drawing.Point(188, 27);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(345, 297);
            this.dataGridView1.TabIndex = 2;
            this.dataGridView1.TabStop = false;
            // 
            // LN_Schluessel
            // 
            this.LN_Schluessel.HeaderText = "Schlüssel";
            this.LN_Schluessel.Name = "LN_Schluessel";
            this.LN_Schluessel.ReadOnly = true;
            // 
            // LN_Wert
            // 
            this.LN_Wert.HeaderText = "Wert";
            this.LN_Wert.Name = "LN_Wert";
            this.LN_Wert.ReadOnly = true;
            this.LN_Wert.Width = 200;
            // 
            // cmd_show
            // 
            this.cmd_show.Location = new System.Drawing.Point(26, 27);
            this.cmd_show.Name = "cmd_show";
            this.cmd_show.Size = new System.Drawing.Size(124, 43);
            this.cmd_show.TabIndex = 3;
            this.cmd_show.Text = "&Hash-Table Anzeigen";
            this.cmd_show.UseVisualStyleBackColor = true;
            this.cmd_show.Click += new System.EventHandler(this.cmd_show_Click);
            // 
            // cmd_one_show
            // 
            this.cmd_one_show.Location = new System.Drawing.Point(26, 108);
            this.cmd_one_show.Name = "cmd_one_show";
            this.cmd_one_show.Size = new System.Drawing.Size(124, 47);
            this.cmd_one_show.TabIndex = 4;
            this.cmd_one_show.Text = "&Bestimmtes Element anzeigen lassen";
            this.cmd_one_show.UseVisualStyleBackColor = true;
            this.cmd_one_show.Click += new System.EventHandler(this.cmd_one_show_Click);
            // 
            // cmd_add_element
            // 
            this.cmd_add_element.Location = new System.Drawing.Point(26, 191);
            this.cmd_add_element.Name = "cmd_add_element";
            this.cmd_add_element.Size = new System.Drawing.Size(124, 44);
            this.cmd_add_element.TabIndex = 5;
            this.cmd_add_element.Text = "E&lement hinzufügen";
            this.cmd_add_element.UseVisualStyleBackColor = true;
            this.cmd_add_element.Click += new System.EventHandler(this.cmd_add_element_Click);
            // 
            // cmd_remove_element
            // 
            this.cmd_remove_element.Location = new System.Drawing.Point(26, 273);
            this.cmd_remove_element.Name = "cmd_remove_element";
            this.cmd_remove_element.Size = new System.Drawing.Size(124, 51);
            this.cmd_remove_element.TabIndex = 6;
            this.cmd_remove_element.Text = "Element ent&fernen";
            this.cmd_remove_element.UseVisualStyleBackColor = true;
            this.cmd_remove_element.Click += new System.EventHandler(this.cmd_remove_element_Click);
            // 
            // Klasse_Hash_Table
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(689, 423);
            this.Controls.Add(this.cmd_remove_element);
            this.Controls.Add(this.cmd_add_element);
            this.Controls.Add(this.cmd_one_show);
            this.Controls.Add(this.cmd_show);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.cmd_clear);
            this.Controls.Add(this.cmd_end);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "Klasse_Hash_Table";
            this.Text = "Klasse Hash-Table";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button cmd_end;
        private System.Windows.Forms.Button cmd_clear;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn LN_Schluessel;
        private System.Windows.Forms.DataGridViewTextBoxColumn LN_Wert;
        private System.Windows.Forms.Button cmd_show;
        private System.Windows.Forms.Button cmd_one_show;
        private System.Windows.Forms.Button cmd_add_element;
        private System.Windows.Forms.Button cmd_remove_element;
    }
}

